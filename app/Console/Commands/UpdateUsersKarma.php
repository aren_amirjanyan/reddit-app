<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\RedditHelper;
use App\User;
use App\Models\UsersKarma;
use Illuminate\Support\Facades\DB;

class UpdateUsersKarma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users_karma:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Users Karma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = DB::table('users')
            ->select('id','reddit_username','refresh_token')
            ->get();
        if (!empty($users)) {
            foreach ($users as $key =>$value) {

                $user_refresh_token = $value->refresh_token;
                $user_id = $value->id;
                $reddit_username = $value->reddit_username;

                $userKarma = UsersKarma::where('user_id',$user_id)
                    ->whereRaw('Date(created_at) = CURDATE()')
                    ->first();
                if(!$userKarma){
                    $redditHelper = new RedditHelper();
                    $access_token = $redditHelper->refreshToken($user_refresh_token)->access_token;
                    $username = $reddit_username;
                    $userData = $redditHelper->getUserData($username,$access_token);

                    if(!empty($userData->data)){
                        $userKarma = new UsersKarma();
                        $userKarma->user_id = $user_id;
                        $userKarma->link_karma = $userData->data->link_karma;
                        $userKarma->comment_karma = $userData->data->comment_karma;
                        $userKarma->created_at = time();
                        $userKarma->updated_at = time();
                        $userKarma->save();
                    }
                }
            }
        }
    }
}
