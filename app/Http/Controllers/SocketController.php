<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class SocketController extends Controller
{
    public function __construct()
    {
       // $this->middleware('guest');
    }
    public function index()
    {
        return view('socket.socket');
    }
    public function writemessage()
    {
        return view('socket.writemessage');
    }
    public function sendMessage(){
        $redis = Redis::connection();
        $redis->publish('message', Request::input('message'));
        return redirect('writemessage');
    }
}
