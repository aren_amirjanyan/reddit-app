<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Http\Requests\GetPostRequest;
use App\Models\ArchivePostsTypes;
use App\User;
use App\Models\Post;
use App\Http\Controllers\Controller;
use App\Services\RedditHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\ArchivePosts;
use App\Models\UsersPosts;
use App\Models\ArchiveSubreddit;
use App\Models\PostsTypes;
use App\Models\UsersKarma;


class AnalyticsController extends Controller
{

    public function index()
    {
        $userKarma = UsersKarma::where('user_id',Auth::user()->id)
            ->whereRaw('Date(created_at) = CURDATE()')
            ->first();
        if(!$userKarma){
            $user_refresh_token = Auth::user()->refresh_token;
            $redditHelper = new RedditHelper();
            $access_token = $redditHelper->refreshToken($user_refresh_token)->access_token;
            $username = Auth::user()->reddit_username;
            $userData = $redditHelper->getUserData($username,$access_token);

            if(!empty($userData->data)){
                $userKarma = new UsersKarma();
                $userKarma->user_id = Auth::user()->id;
                $userKarma->link_karma = $userData->data->link_karma;
                $userKarma->comment_karma = $userData->data->comment_karma;
                $userKarma->created_at = time();
                $userKarma->updated_at = time();
                $userKarma->save();
            }
        }
        $userKarmaAll = UsersKarma::where(['user_id'=>Auth::user()->id])
            ->orderBy('created_at')
            ->get();

        $dataView['post_karma_growth'] = [];
        $dataView['comment_karma_growth'] = [];

        if(!empty($userKarmaAll)){
            foreach($userKarmaAll as $key => $value){
                $date = strtotime($value->created_at);
                $dataView['post_karma_growth'][$key] = [$date,$value->link_karma,Auth::user()->reddit_username," $value->link_karma post karma"];
                $dataView['comment_karma_growth'][$key] = [$date,$value->comment_karma,Auth::user()->reddit_username," $value->comment_karma komment karma"];
            }
        }
        return view('analytics.index',$dataView);
    }
}