<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersKarma extends Model
{
    protected $table = 'users_karma';

    protected $fillable = [
        'user_id',
        'link_karma',
        'comment_karma',
        'created_at',
        'updated_at'
    ];
}