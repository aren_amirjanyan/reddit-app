<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArchivePostsTypes extends Model
{
    protected $table = 'archive_posts_types';

    protected $fillable = [
        'title',
        'description',
        'author_id',
        'created_at',
        'updated_at'];

    public function ArchivePostsTypesToArchivePosts()
    {
        return $this->belongsToMany('App\Models\ArchivePosts', 'posts_types','post_id','type_id');
    }
}