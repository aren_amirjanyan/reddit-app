<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PostsTypes extends Model
{
    protected $table = 'posts_types';

    protected $fillable = [
        'post_id',
        'type_id',
        'user_id',
        'created_at',
        'updated_at'
    ];
    public function PostsTypesToArchivePosts()
    {
        return $this->belongsToMany('App\Models\ArchivePostsTypes','type_id');
    }
}