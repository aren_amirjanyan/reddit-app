@extends('layouts.admin')
@section('content')
    <section id="content">
        <div class="page-loader" style="display: none;"></div>
        @include('partials.title')
        <div class="col-md-10">
            <section class="panel panel-default">
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#archive_subreddits">Subreddits</a>
                        </li>
                        <li><a data-toggle="tab" href="#archive_content_ideas">Content ideas</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="archive_subreddits" class="tab-pane fade in active">
                            <br/>
                            {!! Form::open() !!}
                            <div style="margin-bottom: 60px" class="form-group text-left">
                                {!! Form::label('name', 'Order by') !!}
                                {!! Form::select('type', ['default'=>'Default','new' => 'Date Addeed', 'popular' => 'Popularity'], 'Default',
                                    ['class' => 'form-control chosen-type','id' => 'order_type']) !!}
                                {!! Form::close() !!}
                            </div>
                            <table id="content_subreddits_table" class="table table-striped table-bordered" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>URL</th>
                                    <th>Number of Subscribers</th>
                                    <th>Date added</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>URL</th>
                                    <th>Number of Subscribers</th>
                                    <th>Date added</th>
                                </tr>
                                </tfoot>
                                <tbody id="sub_body">
                                @if(!empty($defaultSubreddits))
                                    @foreach($defaultSubreddits as $value)
                                        <tr>
                                            <td>/{{$value['name']}}</td>
                                            <td><a href="{{$value['url']}}" target="_blank">{{$value['url']}}</a></td>
                                            <td>{{$value['subscribers']}}</td>
                                            <td>{{$value['date_added']}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>

                        <div id="archive_content_ideas" class="tab-pane fade">
                            <br>
                            <!-- Trigger the modal with a button -->
                            <button id="add-archive-post-type" type="button" class="btn btn-success" data-toggle="modal" data-target="#add-archive-post-type-modal">Add Archive Post Type</button>
                            <!-- Modal -->
                            <div id="add-archive-post-type-modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Archive Post Type</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(['id'=>'archive-post-type-form']) !!}
                                            <div class="form-group text-left">
                                                {!! Form::label('name', 'Name of archive post type') !!}
                                                {!! Form::text('name_of_archive_post_type', Input::old('name_of_archive_post_type'),array('id'=>'name-of-archive-post-type','class' => 'form-control','placeholder' => 'Name of archive post type','required' => 'required'))!!}
                                            </div>
                                            <div class="form-group text-left">
                                                {!! Form::label('name', 'Description of archive post type') !!}
                                                {!! Form::textarea('description_of_archive_post_type', Input::old('description_of_archive_post_type'),array('id'=>'description-of-archive-post-type','class' => 'form-control','placeholder' => 'Description of archive post type'))!!}
                                            </div>
                                            <div class="form-group">
                                                <div class="response-message"></div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="save-archive-post-type" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <br/>
                            {!! Form::open() !!}
                            <div class="form-group text-left">
                                {!! Form::label('name', 'Filter by Subreddit') !!}
                                {!! Form::text('filter_by_subreddit', Input::old('filter_by_subreddit'),array('id'=>'filter_by_subreddit','class' => 'form-control chosen-type','placeholder' => 'Write in a subreddit'))!!}
                            </div>
                            <div class="form-group text-left">
                                {!! Form::label('name', 'Filter by User') !!}
                                {!! Form::select('filter_by_user', $authors, '',
                                    ['class' => 'form-control chosen-type','id' => 'filter_by_user']) !!}
                            </div>
                            <div class="form-group text-left">
                                {!! Form::label('name', 'Filter by Type') !!}
                                {!! Form::select('filter_by_type', $types, '',
                                   ['class' => 'form-control chosen-type','id' => 'filter_by_type']) !!}
                            </div>
                            {!! Form::close() !!}
                            <button id="update_list_button" class="btn btn-primary" style="margin-bottom:20px">UPDATE</button>
                            <table id="content_posts_table" class="table table-striped table-bordered" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Subreddit link</th>
                                    <th>User</th>
                                    <th class="no-sort">Type</th>
                                    <th class="no-sort">Command</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Subreddit link</th>
                                    <th>User</th>
                                    <th>Type</th>
                                    <th>Command</th>
                                </tr>
                                </tfoot>
                                <tbody id="arch_content_body">
                                @if(!empty($defaultPosts))
                                    <?php $posts = json_encode($defaultPosts); ?>
                                    @foreach($defaultPosts as $value)
                                        <tr>
                                            <td><a href="{{$value['url']}}" target="_blank">{{$value['url']}}</a></td>
                                            <td>{{$value['user']}}</td>
                                            <td>{{$value['type']}}</td>
                                            <td>
                                                <button type="button" class="btn btn-default post_delete" id="post-id-{{$value['id']}}" data-id="{{$value['id']}}" data-fullname="{{$value['data_id']}}" data-toggle="confirmation">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <?php $posts = json_encode([]); ?>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
    <script>
        var jsonPosts = '<?php echo $posts; ?>';
        var posts = $.parseJSON(jsonPosts);
        var socket = io.connect('{{env('SOCKET_IO_PATH')}}');
        socket.on('message', function (data) {
            var response = $.parseJSON(data);
            if(response.data && response.success == true){
                $.ajax({
                    type: 'get',
                    url: '/get-archive-subreddits-and-posts',
                    data: {},
                    success: function (response) {
                        if(response.data && response.success == true){

                                var subreddits = response.data.subreddits;
                                var subredditsTable = $('#content_subreddits_table').DataTable();
                                subredditsTable.clear();
                                var dataSubreddits = [];
                                $.each(subreddits, function(key,value){
                                    dataSubreddits.push([value.name,'<a href="'+value.url+'" target="_blank">'+value.url+'</a>',value.subscribers,value.date_added]);
                                });
                                subredditsTable.rows.add(dataSubreddits);
                                subredditsTable.draw();

                                posts = response.data.posts;
                                var postsTable = $('#content_posts_table').DataTable();
                                postsTable.clear();
                                var dataPosts = [];
                                $.each(posts, function(key,value){
                                    dataPosts.push(['<a href="'+value.url+'" target="_blank">'+value.url+'</a>',value.user,value.type,'<button type="button" class="btn btn-default post_delete" id="post-id-'+value.id+'" data-id="'+value.id+'" data-fullname="'+value.data_id+'" data-toggle="confirmation">Delete</button>']);
                                });
                                postsTable.rows.add(dataPosts);
                                postsTable.draw();
                            var authors = response.data.authors;
                            $('#filter_by_user').empty();
                            $.each(authors, function(key, value) {
                                $('#filter_by_user')
                                        .append($("<option></option>")
                                                .attr("value",key)
                                                .text(value));
                            });

                            var types = response.data.types;
                            $('#filter_by_type').empty();
                            $('#filter_by_type')
                                    .append($("<option></option>")
                                            .attr("value",'default')
                                            .text('By All Types'));
                            $.each(types, function(key, value) {
                                $('#filter_by_type')
                                        .append($("<option></option>")
                                                .attr("value",key)
                                                .text(value));
                            });
                        }
                    }
                });
            }
        });
    </script>
@endsection