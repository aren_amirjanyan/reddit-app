@extends('layouts.admin')
@section('content')
    <section id="content">
        <div class="page-loader" style="display: none;"></div>
        @include('partials.title')
        <div class="col-md-12">
            {!! Form::open(array('route'=>'generateReports','class'=>'form-horizontal')) !!}
            {!! Form::submit('GENERATE REPORTS',['class' => 'btn btn-primary','id' => 'generate_reports']) !!}
            <div class="form-group">
                <div class="text-center">
                    <h3>Post Karma Growth</h3>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    {!! Form::label('name', 'Show by') !!}
                    {!! Form::select('show_post_karma_by', [
                    'all' => 'All Time',
                    'week' => 'This Week',
                    'month' => 'This Month',
                    'six_months' => 'Last 6 Months'], 'all',
                    ['class' => 'form-control chosen-type','id' => 'show_post_karma_by']) !!}
                </div>
                <div class="col-md-10">
                    <div id="post_karma" style="width: 100%;height: 500px;"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <h3>Comment Karma Growth</h3>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    {!! Form::label('name', 'Show by') !!}
                    {!! Form::select('show_comment_karma_by', [
                    'all' => 'All Time',
                    'week' => 'This Week',
                    'month' => 'This Month',
                    'six_months' => 'Last 6 Months'], 'all',
                    ['class' => 'form-control chosen-type','id' => 'show_comment_karma_by']) !!}
                </div>
                <div class="col-md-10">
                    <div id="comment_karma" style="width: 100%;height: 500px;"></div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <br><br>
        <script>
            var showChart = function (data, chartContent, typeOfKarma) {

                var karma_data = data;

                this.init = function () {

                    google.charts.load('current', {'packages': ['line', 'annotatedtimeline']});

                    google.charts.setOnLoadCallback(drawChart);

                };
                function drawChart() {
                    var date_for_chart = [];
                    $.each(karma_data, function (key, value) {
                        var date = new Date(parseInt(value[0]) * 1000);
                        date_for_chart.push(date);
                        karma_data[key][0] = date;
                    });
                    var karma_data_full = new google.visualization.DataTable();
                    karma_data_full.addColumn('date', 'Month');
                    karma_data_full.addColumn('number', "Count of " + typeOfKarma + " Karma");
                    karma_data_full.addColumn('string', 'username');
                    karma_data_full.addColumn('string', 'count_of_karma');
                    karma_data_full.addRows(karma_data);

                    var classicOptions = {
                        displayAnnotations: true
                    };

                    drawClassicChart(karma_data_full, chartContent, classicOptions);
                }

                function drawClassicChart(data, chartDiv, classicOptions) {
                    var chart = new google.visualization.AnnotatedTimeLine(chartDiv);
                    chart.draw(data,classicOptions);
                }

            };

            post_karma_growth = '<?php echo json_encode($post_karma_growth);?>';
            comment_karma_growth = '<?php echo json_encode($comment_karma_growth);?>';

            var typeOfCommentKarma = $('#show_comment_karma_by').val();
            var typeOfPostKarma = $('#show_post_karma_by').val();

            showResultOfCharts(typeOfPostKarma, post_karma_growth, 'post_karma', 'Post');

            showResultOfCharts(typeOfCommentKarma, comment_karma_growth, 'comment_karma', 'Comment');

            $(document).on('change', '#show_post_karma_by', function () {
                var typeOfdata = $(this).val();
                showResultOfCharts(typeOfdata, post_karma_growth, 'post_karma', 'Post');
            });

            $(document).on('change', '#show_comment_karma_by', function () {
                var typeOfdata = $(this).val();
                showResultOfCharts(typeOfdata, comment_karma_growth, 'comment_karma', 'Comment');
            });

            function showResultOfCharts(typeOfdata, karmaGrowth, chartContentId, typeOfKarma) {
                var data = $.parseJSON(karmaGrowth);
                var dataWithFilter = filterBy(typeOfdata, data);
                filteringChartData(dataWithFilter, chartContentId, typeOfKarma);
            }

            function filterByWeek(growthData) {
                var data = [];
                var current_date = new Date();
                $.each(growthData, function (key, value) {
                    var date = new Date(parseInt(value[0]) * 1000);
                    if (
                            date.getYear() == current_date.getYear() &&
                            date.getMonth() == current_date.getMonth() &&
                            date.getDay() >= 0 &&
                            date.getDay() <= 6 &&
                            date.getDate() <= current_date.getDate() + (6 - date.getDay()) &&
                            date.getDate() >= current_date.getDate() - (6 - date.getDay())
                    ) {
                        data.push(value);
                    }
                });
                return data;
            }
            function filterByMonth(growthData) {
                var data = [];
                var current_date = new Date();
                $.each(growthData, function (key, value) {
                    var date = new Date(parseInt(value[0]) * 1000);
                    if (date.getMonth() == current_date.getMonth()) {
                        data.push(value);
                    }
                });
                return data;
            }
            function filterBySixMonth(growthData) {
                var year;
                var lastSixMothsStart;
                var data = [];
                var current_date = new Date();
                if (current_date.getMonth() - 6 < 0) {
                    year = current_date.getYear() - 1;
                    lastSixMothsStart = 6 + current_date.getMonth();
                } else {
                    lastSixMothsStart = current_date.getMonth() - 6;
                    year = current_date.getYear();
                }
                $.each(growthData, function (key, value) {
                    var date = new Date(parseInt(value[0]) * 1000);
                    if (date.getYear() >= year && (date.getMonth() >= lastSixMothsStart || date.getMonth() <= current_date.getMonth())) {
                        data.push(value);
                    }
                });
                return data;
            }
            function filterByAll(growthData) {
                var data = [];
                data = growthData;
                return data;
            }

            function filterBy(filterType, growthData) {
                var data = [];
                switch (filterType) {
                    case 'week':
                    {
                        data = filterByWeek(growthData);
                        break;
                    }
                    case 'month':
                    {
                        data = filterByMonth(growthData);
                        break;
                    }
                    case 'all':
                    {
                        data = filterByAll(growthData);
                        break;
                    }
                    case 'six_months':
                    {
                        data = filterBySixMonth(growthData);
                        break;
                    }
                }
                return data;
            }
            function filteringChartData(data, chartContentId, typeOfKarma) {
                var chartContent = document.getElementById(chartContentId);
                var chart = new showChart(data, chartContent, typeOfKarma);
                chart.init();
            }

        </script>
    </section>
@endsection